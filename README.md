# Next Gen Bible

## Project front end setup
```
npm install
```

### Compiles and minifies for production
```
npm run build
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Lints and fixes files
```
npm run lint
```

## Project backend setup
```
pip install requirements.txt
python run.py
```



### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
