from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from sqlalchemy import MetaData

mail = Mail()
db = SQLAlchemy()
metadata = MetaData()