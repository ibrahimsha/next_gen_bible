import os

from flask import Flask, send_from_directory, render_template
from flask_migrate import Migrate
from flask_cors import CORS
from .plugins import db

def create_app():
    
    app = Flask(__name__,static_folder="../dist")
    
    # print(app.static_folder)
    app.config.from_pyfile('settings.py', silent=True)
    plugins(app)
    CORS(app, resources={r'/*': {'origins': '*'}})
    
    from .api import api
    app.register_blueprint(api,url_prefix='/api')

    return app


def plugins(app):
    db.init_app(app)
    Migrate(app,db)
    return None

app = create_app()

# Serve Vue App
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def serve(path):
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')
