import os
basedir = os.path.abspath(os.path.dirname(__file__))

# DB Settings development
SECRET_KEY = 'you-will-never-guess'
# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir+"/../", 'app.db')
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:root@localhost/biblica2'
                        #    mysql+mysqldb://scott:tiger@localhost/foo
SQLALCHEMY_TRACK_MODIFICATIONS = False

# APP Settings development
APP_NAME = 'Next Gen Bible'

# SERVER_NAME = 'localhost:8000'

# Flask-Mail.
MAIL_DEFAULT_SENDER = 'contact@local.host'
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = False
MAIL_USERNAME = 'ibrahimshafyn@gmail.com'
MAIL_PASSWORD = ''

# Celery.
CELERY_BROKER_URL = 'redis://:devpassword@redis:6379/0'
CELERY_RESULT_BACKEND = 'redis://:devpassword@redis:6379/0'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_REDIS_MAX_CONNECTIONS = 5
