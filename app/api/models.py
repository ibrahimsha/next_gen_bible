from app import db, create_app
from app.plugins import metadata
from sqlalchemy import Table

app = create_app()

with app.app_context():    
    
    table_reflection = Table("trans_engwyc2018", metadata, autoload=True, autoload_with=db.engine)
    attrs = {"__table__": table_reflection}
    EnglishBase = type("trans_engwyc2018", (db.Model,), attrs)