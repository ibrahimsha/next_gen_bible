import json
from flask import jsonify, make_response
from app.api import api
from .models import EnglishBase

from app.lib import AlchemyEncoder

@api.route('/ping',methods=['GET'])
def index():
    verses = EnglishBase.query.filter(
        EnglishBase.part_id == 1,
        EnglishBase.chapter_id == 1
        ).all()
    data = json.dumps(verses, cls=AlchemyEncoder)
    print(data)
    response = make_response(data)
    response.headers["Content-Type"] = "application/json"
    return response